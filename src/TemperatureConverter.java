public enum TemperatureConverter {
    C_F("C", "F") {
        @Override
        float convert(float temperature) {
            return (temperature * 9) / 5 + 32f;
        }
    },
    C_K("C", "K") {
        @Override
        float convert(float temperature) {
            return temperature + 273.15f;
        }
    },
    K_C("K", "C") {
        @Override
        float convert(float temperature) {
            return temperature - 273.15f;
        }
    },
    F_C("F", "C") {
        @Override
        float convert(float temperature) {
            return (temperature - 32f) * 5 / 9;
        }
    },
    F_K("F", "K") {
        @Override
        float convert(float temperature) {
            return (temperature + 459.67f) * 5 / 9;
        }
    },
    K_F("K", "F") {
        @Override
        float convert(float temperature) {
            return temperature * 9 / 5 - 459.67f;
        }
    };

    private final String inputUnit;
    private final String outputUnit;
    abstract float convert(float temperature);


    TemperatureConverter(final String inputUnit, final String outputUnit) {
        this.inputUnit = inputUnit;
        this.outputUnit = outputUnit;
    }

    public static float convertTemperature(final String inputUnit, final String outputUnit, final float temperature) {
        float convertedValue = 0;
        for (TemperatureConverter element : values()) {
            if (element.inputUnit == inputUnit && element.outputUnit == outputUnit) {
                convertedValue = element.convert(temperature);
            }
        }
        return convertedValue;
    }
}